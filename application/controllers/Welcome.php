<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	var $province = 'provinces_data';
	var $city = 'cities_data';
	var $type_service = array(
		'' => 'Pilih Kurir',
		'jne' => 'JNE',
		'tiki' => 'TIKI',
		'pos' => 'POS Indonesia',
		'jnt' => 'J&T',
	);

	function __construct(){
		parent::__construct();
		$this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file', 'key_prefix' => 'my_'));
		$this->load->library('rajaongkir');
	}

	public function index()
	{
		$this->load->library('Layouts');

		$location_data = $this->getDataLocation();

		$provinces 	= $location_data['provinces'];
		$cities 	= $location_data['cities'];

		$cities = $this->cityShaping($cities);

		$Layouts = new Layouts();

		$Layouts->view('main_app/default', array(
			'provinces'	=> $provinces,
			'cities'   	=> $cities,
			'type_service' => $this->type_service
		), 'default');
	}

	public function city($province_id = null, $city_id = null, $view = true)
	{
		$location_data = $this->getDataLocation();

		$cities = $location_data['cities'];

		$result = array();
		if(!empty($province_id) && isset($cities[$province_id])){
			$result[$province_id] = $cities[$province_id];
		}else{
			$result = $cities;
		}

		$cities = $this->cityShaping($result);

		if(!empty($cities[$city_id])){
			return $cities[$city_id];
		}else{
			if($view){
				$this->load->library('Layouts');

				$Layouts = new Layouts();

				$Layouts->view('elements/city', array('cities' => $cities), 'ajax');
			}else{
				return $cities;
			}
		}
	}

	public function cost()
	{
		$this->load->library('Layouts');

		$Layouts = new Layouts();

		$carts = (array) $this->session->userdata('cart_item');

		/* origin jakarta timur*/
		$origin 		= 154;
		$province 		= $this->input->get('province');
		$destination 	= $this->input->get('city');
		$weight 		= $this->input->get('weight');
		$courier 		= $this->input->get('courier');

		// if(empty($province) || empty($destination) || empty($weight) || empty($courier)){
		// 	$this->session->set_flashdata('success', 'Isi semua form');

		// 	redirect('/');
		// }

		if(!empty($destination)){
			$costs = $this->rajaongkir->cost($origin, $destination, $weight, $courier);
			$costs = json_decode($costs);
		}else{
			$costs = array();
		}
		
		$location_data = $this->getDataLocation();

		$provinces 	= $location_data['provinces'];
		$cities 	= $location_data['cities'];

		$result = array();

		if(isset($cities[$province])){
			$result[$province] = $cities[$province];
		}else{
			$result = $cities;
		}

		$cities = $this->cityShaping($result);

		$Layouts->view('main_app/cost', array(
			'provinces'		=> $provinces,
			'cities'   		=> $cities,
			'costs'   		=> $costs,
			'courier_list'	=> isset($costs->rajaongkir->results) ? $costs->rajaongkir->results : array(),
			'data_input'	=> array(
				'province' 		=> $province,
				'destination' 	=> $destination,
				'weight' 		=> $weight,
				'courier' 		=> $courier,
				'origin' 		=> $origin,
			),
			'type_service' => $this->type_service,
			'carts'			=> $carts
		), 'default');
	}

	public function clear_cache()
	{
		$this->session->set_flashdata('success', 'Berhasil memperbaharui cache');

		$this->output->delete_cache('/');

		$this->getDataLocation(false);

		redirect($_SERVER['HTTP_REFERER']);
	}

	private function cityShaping($data = array())
	{
		$result = array();
		foreach ($data as $key => $value) {
			foreach ($value as $key => $val) {
				$lower = strtolower($val['type']);
				if($lower == 'kota'){
					$text = sprintf('%s %s', $val['type'], $val['city_name'] );
				}else{
					$text = $val['city_name'];
				}

				$result[$val['city_id']] = $text;
			}
		}

		return $result;
	}

	private function getDataLocation($with_return = true)
	{
		$provinces = $this->cache->get($this->province);

		if(empty($provinces)){
			$provinces = $this->rajaongkir->province();

			$provinces = json_decode($provinces);
			$temp = array();
			foreach ($provinces->rajaongkir->results as $key => $value) {
				$temp[$value->province_id] = $value->province;
			}
			$provinces = $temp;

			$this->cache->save($this->province, $provinces);
		}

		$city = $this->cache->get($this->city);
		if(empty($city)){
			$city = $this->rajaongkir->city();

			$city = json_decode($city);

			$temp = array();
			foreach ($city->rajaongkir->results as $key => $value) {
				$temp[$value->province_id][$value->city_id] = (array) $value;
			}

			$city = $temp;

			$this->cache->save($this->city, $city);
		}

		if($with_return){
			return array(
				'provinces' => $provinces,
				'cities' => $city,
			);
		}
	}

	private function fullJsonToArray($data)
	{
		$result = array();
		foreach ($data as $key => $value) {
			$result[$key] = (array) $value;
		}

		return $result;
	}

	public function print()
	{
		$carts = (array) $this->session->userdata('cart_item');

		if(empty($carts)){
			$this->session->set_flashdata('error', 'tidak ada data ditemukan');

			redirect('/');
		}

		$this->load->library('Layouts');

		$Layouts = new Layouts();

		$Layouts->view('main_app/print', array(
			'carts' 		=> $carts
		), 'print');
	}

	public function cart()
	{
		$origin 		= $this->input->get('origin');
		$province 		= $this->input->get('province');
		$destination 	= $this->input->get('destination');
		$weight 		= $this->input->get('weight');
		$courier 		= $this->input->get('courier');

		$package_type 	= $this->input->get('package_type');
		$receiver_name 	= $this->input->get('receiver_name');
		$phone 			= $this->input->get('phone');
		$address 		= $this->input->get('address');
		$deskripsi 		= $this->input->get('deskripsi');

		if(empty($origin) || empty($province) || empty($destination) || empty($weight) || empty($courier) || empty($package_type) || empty($receiver_name) || empty($phone) || empty($address) || empty($deskripsi)){
			$this->session->set_flashdata('error', 'Isi semua form');

			redirect('/');
		}

		$data = array(
			'origin' 		=> $origin,
			'province' 		=> $province,
			'destination' 	=> $destination,
			'weight' 		=> $weight,
			'courier' 		=> $courier,
			'package_type' 	=> $package_type,
			'receiver_name' => $receiver_name,
			'phone' 		=> $phone,
			'address' 		=> $address,
			'deskripsi' 	=> $deskripsi,
		);

		$strtotime = strtotime(date('Y-m-d H:i:s'));

		$carts = (array) $this->session->userdata('cart_item');

		$carts[$strtotime] = $data;

		$this->session->set_userdata('cart_item', $carts);

		redirect('/');
	}

	public function delete_cart($id = '')
	{
		if(!empty($id)){
			$carts = (array) $this->session->userdata('cart_item');

			unset($carts[$id]);

			$this->session->set_userdata('cart_item', $carts);
		}else{
			$this->session->set_userdata('cart_item', array());
		}

		redirect('/');
	}
}
