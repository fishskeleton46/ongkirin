<div class="row justify-content-md-center">
	<div class="col-md-auto">
		<div class="card mt-4" style="width: 18rem;">
			<div class="card-header">
				<h1>Ongkir.in</h1>
			</div>
			<div class="card-body">
				<form action="welcome/cost" method="GET">
					<div class="form-group">
						<label for="">Kurir</label>
						<?php 
							echo form_dropdown('courier', $type_service, set_value('courier', ''), array(
								'class' => 'form-control change-courier',
							));
						?>
					</div>
					<div class="form-group">
						<label for="">Provinsi</label>
						<?php 
							echo form_dropdown('province', $provinces, set_value('province', ''), array(
								'class' => 'form-control change-province',
							));
						?>
					</div>
					
					<?php $this->view('elements/city'); ?>

					<div class="form-group">
						<label for="">Berat <small>(gram)</small></label>
						<?php 
							echo form_input('weight', '', array(
								'class' => 'form-control',
							));
						?>
					</div>
					<div class="form-group">
						<button>Submit</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<script>
	$(document).ready(function(){
		var location_func = function(){
			$('.change-province').off('change');
			$('.change-province').change(function(){
				var val = $(this).val();
				var target = '#city-box';
				$.ajax({
					url: 'welcome/city/'+val,
					type: 'GET',
					success: function(data) {
						var content = $(data).filter(target);

						$(target).html(content.html());

						location_func();
					},
					error: function(e) {
						alert('error deleting data')
					}
				});
			});
		}

		location_func();
	});
</script>