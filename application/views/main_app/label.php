<div class="col-md-6">
	<div class="rounded" style="border: 3px dashed #ccc;padding: 15px;">
		<table class="table table-bordered mb-0">
			<tr>
				<td style="vertical-align: top;">
					<div class="float-left">
						<img src="<?php echo base_url() ?>/assets/img/babycocoa.png" alt="" style="height: 80px;">
					</div>
					<?php 
							switch ($courier) {
								case 'jne':
									$image_kurir = 'jne.jpg';
								break;
								case 'tiki':
									$image_kurir = 'tiki.png';
								break;
								case 'pos':
									$image_kurir = 'pos.png';
								break;
								case 'jnt':
									$image_kurir = 'jnt.png';
								break;
							}
					?>
					<div class="d-block text-right">
						<img src="<?php echo base_url() ?>/assets/img/<?php echo $image_kurir?>" alt="" style="width: 15%;">
						<span class="mt-2 d-block" style="font-size: 14px;"><?php echo $package_type ?></span>
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<p class="mb-0"><b>Pengirim :</b> @babycocoa.id</p>
					<p class="mb-0">JAKARTA TIMUR</p>
					<div class="d-block mt-3">
						<i class="fab fa-whatsapp"></i> 081386801043
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<p class="mb-0"><b>Penerima :</b> <?php echo $receiver_name ?></p>
					<p class="mb-0"><b>Alamat :</b> <?php echo $address ?></p>
					<p class="mb-0"><b>Telepon :</b> <?php echo $phone ?></p>
				</td>
			</tr>
			<tr>
				<td>
					<p class="mb-0"><b>Deskripsi :</b> <?php echo $deskripsi ?></p>
				</td>
			</tr>
			<tr>
				<td>
					<div class="font-weight-bold">
						<i class="fab fa-instagram"></i> Follow Instagram
						<span class="float-right">@babycocoa.id | @bleutiful_id</span>
					</div>
				</td>
			</tr>
		</table>
	</div>
</div>