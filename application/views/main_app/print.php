<a href="/" class="d-print-none btn btn-secondary ml-2 my-2">Back</a>
<button type="button" class="d-print-none btn btn-success ml-1 my-2 print-func">print</button>
<?php 
	$idx = 0;
	foreach ($carts as $key => $value) {
		if($idx % 2 == 0){
			echo '<div class="mb-3 row">';
		}

		$this->view('main_app/label', $value);

		if($idx % 2 == 1){
			echo '</div>';
		}

		$idx++;

		if($idx % 6 == 0){
			echo '<div style="page-break-before: always;"></div>';
		}
	}

	if($idx > 0){
		echo '</div>';
	}
?>
<script>
	$(document).ready(function(){
		$('.print-func').click(function(){
			window.print();

			return false;
		});
	});
</script>