<?php 
	$province_val  		= isset($data_input['province']) ? $data_input['province'] : '';
	$courier_val     	= isset($data_input['courier']) ? $data_input['courier'] : '';
	$weight_val     	= isset($data_input['weight']) ? $data_input['weight'] : '';
	$destination_val  	= isset($data_input['destination']) ? $data_input['destination'] : '';

	$nama_provinsi 		= isset($provinces[$province_val]) ? $provinces[$province_val] : '--nama provinsi--';
	$nama_kota 			= isset($cities[$destination_val]) ? $cities[$destination_val] : '--nama kota--';
?>
<div class="row justify-content-md-center mb-4">
	<div class="col-md-auto">
		<div class="card mt-4" style="width: 48rem;">
			<div class="card-header">
				<h1>Ongkir.in</h1>
			</div>
			<div class="card-body">
				<?php if($this->session->flashdata('error')){ ?>
				<div class="alert alert-danger" role="alert">
					<?php echo $this->session->flashdata('error'); ?>
				</div>
				<?php } ?>
				<?php if($this->session->flashdata('success')){ ?>
				<div class="alert alert-success" role="alert">
					<?php echo $this->session->flashdata('success'); ?>
				</div>
				<?php } ?>
				<div class="row">
					<div class="col-md-3">
						<form action="<?php echo base_url();?>welcome/cost" method="GET">
							<div class="form-group">
								<label for="">Kurir</label>
								<?php 
									echo form_dropdown('courier', $type_service, set_value('courier', $courier_val), array(
										'class' => 'form-control change-courier',
									));
								?>
							</div>
							<div class="form-group">
								<label for="">Provinsi</label>
								<?php 
									echo form_dropdown('province', $provinces, set_value('province', $province_val), array(
										'class' => 'form-control change-province',
									));
								?>
							</div>
							
							<?php $this->view('elements/city'); ?>

							<div class="form-group">
								<label for="">Berat <small>(gram)</small></label>
								<?php 
									echo form_input('weight', set_value('weight', $weight_val), array(
										'class' => 'form-control',
									));
								?>
							</div>
							<div class="form-group">
								<button type="submit" class="btn btn-primary btn-block">Cek Harga</button>
							</div>
						</form>
					</div>
					<div class="col-md-9">
						<?php 
							$hidden = array(
								'origin' 		=> $data_input['origin'],
								'destination'	=> $destination_val,
								'province' 		=> $province_val,
								'weight' 		=> $weight_val,
								'courier' 		=> $courier_val,
							);

							echo form_open('welcome/cart', array(
								'method' => 'get'
							), $hidden);
						?>
							<?php
								$idx = 0;

								if(!empty($courier_list)){
									foreach ($courier_list as $key => $courier_val) {
							?>
							<h2><?php echo $courier_val->name ?></h2>
							<?php
										foreach ($courier_val->costs as $key => $val) {
											$val_cost = $val->cost[0];
											
											$name_package = $val->service;
											$desc = $name_package;
											if($val->service != $val->description){
												$desc = sprintf('%s (<b>%s</b>)', $val->description, $val->service);
												$name_package = sprintf('%s (%s)', $val->description, $val->service);
											}

											$text = sprintf('%s - <span style="text-decoration: underline;font-style: italic;">Rp.%s<span>', $desc, number_format($val_cost->value));

							?>
							<div class="custom-control custom-radio">
								<input type="radio" name="package_type" value="<?php echo $name_package ?>" class="custom-control-input" id="customCheckDisabled<?php echo $idx;?>">
								<label class="custom-control-label" for="customCheckDisabled<?php echo $idx;?>"><?php echo $text ?></label>
							</div>
							<?php
											$idx++;
										}
									}

									echo '<hr>';
								}else{
							?>
							<div class="form-group">
								<label for="">Nama Tipe Paket</label>
								<?php 
									echo form_input('package_type', '', array(
										'class' => 'form-control',
										'autocomplete' => 'off',
									));
								?>
							</div>
							<?php
								}
							?>
							<div class="form-group">
								<label for="">Nama Penerima</label>
								<?php 
									echo form_input('receiver_name', '', array(
										'class' => 'form-control',
										'autocomplete' => 'off',
									));
								?>
							</div>
							<div class="form-group">
								<label for="">No.Handphone</label>
								<?php 
									echo form_input('phone', '', array(
										'class' => 'form-control',
										'autocomplete' => 'off',
									));
								?>
							</div>
							<div class="form-group">
								<label for="">Alamat Lengkap</label>
								<?php 
									echo form_input('address', sprintf('--alamat--, %s, %s, --kode pos--', $nama_kota, $nama_provinsi), array(
										'class' => 'form-control',
										'autocomplete' => 'off',
										'rows' => '5'
									));
								?>
							</div>
							<div class="form-group">
								<label for="">Deskripsi</label>
								<?php 
									echo form_input('deskripsi', 'Baju Bayi', array(
										'class' => 'form-control',
										'autocomplete' => 'off',
									));
								?>
							</div>
							<div class="form-group">
								<button type="submit" class="btn btn-primary float-right">Submit</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		
		<?php 
			if(!empty($carts)){
		?>
		<div class="card mt-4">
			<div class="card-header">
				<h6>Cart <a href="<?php echo site_url('welcome/print') ?>" class="float-right btn btn-info btn-sm">print</a> <a href="<?php echo site_url('welcome/delete_cart') ?>" class="float-right btn btn-danger btn-sm mr-2">remove</a></h6>
			</div>
			<div class="card-body">				
				<?php 
					foreach ($carts as $key => $value) {
						switch ($value['courier']) {
							case 'jne':
								$image_kurir = 'jne.jpg';
							break;
							case 'tiki':
								$image_kurir = 'tiki.png';
							break;
							case 'pos':
								$image_kurir = 'pos.png';
							break;
							case 'jnt':
								$image_kurir = 'jnt.png';
							break;
						}
				?>
				<div>
					<div class="float-left">
						<h4><?php echo $value['receiver_name']; ?></h4>
						<p><span>Alamat: </span> <?php echo $value['address']; ?></p>
						<a href="<?php echo site_url('welcome/delete_cart/'.$key) ?>" class="btn btn-danger">hapus</a>
					</div>
					<div class="float-right text-right">
						<img src="<?php echo base_url() ?>/assets/img/<?php echo $image_kurir; ?>" alt="" style="max-width: 70px;">
						<span class="mt-2 d-block" style="font-size: 14px;"><?php echo $value['package_type']; ?></span>
					</div>
					<div class="clearfix"></div>
				</div>
				<hr>
				<?php 
					}
				?>
			</div>
		</div>
		<?php 
			}
		?>
	</div>
</div>
<script>
	$(document).ready(function(){
		var location_func = function(){
			$('.change-province').off('change');
			$('.change-province').change(function(){
				var val = $(this).val();
				var target = '#city-box';
				$.ajax({
					url: '<?php echo base_url();?>welcome/city/'+val,
					type: 'GET',
					success: function(data) {
						var content = $(data).filter(target);

						$(target).html(content.html());

						location_func();
					},
					error: function(e) {
						alert('error deleting data')
					}
				});
			});
		}

		location_func();
	});
</script>