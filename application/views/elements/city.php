<?php 
	$destination_val  	= isset($data_input['destination']) ? $data_input['destination'] : '';
?>
<div class="form-group" id="city-box">
	<label for="">Kota</label>
	<?php 
		echo form_dropdown('city', $cities, set_value('city', $destination_val), array(
			'class' => 'form-control change-city',
		));
	?>
</div>