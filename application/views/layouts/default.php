<!DOCTYPE html>
<html lang="en">

	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width,initial-scale=1">
		<title>Ongkirin <?php echo $title_for_layout;?></title>

		<link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">

		<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
		<script src="<?php echo base_url();?>assets/js/jquery-3.4.1.min.js"></script>
	</head>

	<body>
		<div class="container">
			<?php echo $content_for_layout;?>
		</div>
	</body>

</html>